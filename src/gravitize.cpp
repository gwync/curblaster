#include "game_object.h"

game_object gravitize(game_object object){

  if(object.vspeed<4&&object.vspeed>0&&object.vtime%4==0){
    object.vspeed = object.vspeed + 1;
  };

  object.x = object.x + object.vspeed;
  object.vtime++;

  return object;
}