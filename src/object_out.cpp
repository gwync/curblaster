#include "game_object.h"

game_object object_out(game_object object){
  object.active=0;
  object.x = -1;
  object.y = -1;
  object.speed = 0;
  object.direction = 0;
  object.chase = -1;
  object.phase = 0;
  return object;
}