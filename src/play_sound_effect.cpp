#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

int play_sound_effect(Mix_Chunk *chunk, int channel){
  channel = Mix_PlayChannel(-1, chunk, 0);
  return channel;
};