#include <ncurses.h>
#include <cstdlib>

#include "game_object.h"

int draw_object(game_object object, int drawlocation){

  if(object.number==1){
    if((object.y-drawlocation<35) and (object.direction==4)){
      drawlocation=drawlocation-object.speed;
    };
    if((object.y-drawlocation>40) and (object.direction==6)){
      drawlocation=drawlocation+object.speed;
    };
  };
    
  int offset = 0;

  offset = object.y-drawlocation;

  if(drawlocation>=540&&object.y<620-drawlocation&&object.number!=1){
    offset = 620-drawlocation+object.y;  //right of player wrap correction
  };
    
  if(drawlocation<0&&object.y>=540&&object.number!=1){
    offset = abs(drawlocation)-abs(620-object.y);  //left of player wrap correction
  };
    
  if(object.number>=110&&object.number<=121&&object.phase==1){ attron(A_BOLD); };
  if((object.active==1&&object.x>=1&&object.x<=18&&offset>=0&&offset<=79)||((object.number>=200&&object.number<=230)&&(offset>=0&&offset<=79))){
    if(object.face==0){
      mvprintw(object.x,offset,"%s",object.line0);
    } else if(object.face==1){
      mvprintw(object.x,offset,"%s",object.line1);
    } else if(object.face==2){
      mvprintw(object.x,offset,"%s",object.line2);
    } else if(object.face==3){
      mvprintw(object.x,offset,"%s",object.line3);
    } else if(object.face==4){
      mvprintw(object.x,offset,"%s",object.line1);
      mvprintw(object.x-1,offset,"%s",object.line0);
    } else if(object.face==5){
      mvprintw(object.x,offset,"%s",object.line1);
      mvprintw(object.x-1,offset,"%s",object.line0);
    } else if(object.face==6){
      mvprintw(object.x,offset,"%s",object.line3);
      mvprintw(object.x-1,offset,"%s",object.line2);
    };
  };
  if(object.number>=110&&object.number<=121){ attroff(A_BOLD); };
  return drawlocation;

}

void draw_board(int score, int lives, int level, int shieldsleft, int missile){

  // mvprintw(row,col,string,vars)
  //top
  mvprintw(0,0,"--------------------------------------------------------------------------------\n");
  //bottom
  mvprintw(19,0,"--------------------------------------------------------------------------------\n");
  //left side of map
  mvprintw(20,16,"|");
  mvprintw(21,16,"|");
  mvprintw(22,16,"|");
  mvprintw(23,16,"|");
  //right side of map
  mvprintw(20,79,"|");
  mvprintw(21,79,"|");
  mvprintw(22,79,"|");
  mvprintw(23,79,"|");
  //visible box on map 33, 43
  mvprintw(20,42,"/");
  mvprintw(20,51,"\\");
  mvprintw(23,42,"\\");
  mvprintw(23,51,"/");
  //Score
  mvprintw(23,1,"Score:%8d",score);
  //Lives
  mvprintw(20,1,"Lv:");
  int count = lives;
  for(int x=4;count>0;x=x+2){
    mvprintw(20,x,"<>");
    count--;
  };
  //Missles
  count = missile;
  for(int x=13;count>0;x=x+1){
    mvprintw(20,x,"M");
    count--;
  };

  //Level
  mvprintw(22,1,"Lvl:%3d", level);
  mvprintw(22,9,"S:%3d",  shieldsleft);
}

void display_controls(){
  mvprintw(1,21,"Movement: Arrow keys");
  mvprintw(1,46,"Shoot: Z");
  mvprintw(2,46,"Shield toggle: S");
  mvprintw(3,46,"Smart bomb: A");
  mvprintw(2,21,"Pause: Space");
  mvprintw(3,21,"Quit: Q");
  mvprintw(5,18,"See \'man curblaster\' for complete documentation");
};
