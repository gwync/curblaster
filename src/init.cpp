#include <cstdlib>

#include "game_object.h"

game_object pod_init(game_object pod){
  pod.active=1;
  pod.x = 18;  //always start on ground
  pod.y = rand()%620; //anywhere along the surface
  pod.direction = rand()%1000;
  if(pod.direction<500){
    pod.direction = 4;
  } else {
    pod.direction = 6;
  };
  pod.speed = 1;

  return pod;
}

game_object lander_init(game_object lander){
  lander.active=1;
  lander.x = rand()%17;
  lander.y = rand()%600+20;
  lander.direction = rand()%1000;
  if(lander.direction<500){
    lander.direction = 4;
  } else {
    lander.direction = 6;
  };
  lander.speed = 1;

  return lander;
}

game_object crawler_init(game_object crawler){
  crawler.active=1;
  crawler.x = 18;
  crawler.y = rand()%600+20;
  crawler.direction = rand()%1000;
  if(crawler.direction<500){
    crawler.direction = 4;
  } else {
    crawler.direction = 6;
  };
  crawler.speed = 1;

  return crawler;
}

game_object boss_init(game_object boss, int level){
  boss.active=1;
  boss.x = rand()%17;
  boss.y = rand()%600+20;
  boss.direction = rand()%1000;
  if(boss.direction<500){
    boss.direction = 4;
  } else {
    boss.direction = 6;
  };
  boss.speed = 1;
  boss.phase = level * 5;

  return boss;
}

game_object player_init(game_object player){
  player.x = 18;
  player.y = rand()%600+20;
  player.direction = 5;
  player.face = 1;
  player.speed = 0;
  player.vspeed = 0;
  player.vtime = 0;

  return player;
}

game_object missile_init(game_object missile, const game_object& player, const game_object& lander){
  missile.active=1;
  missile.x = player.x;
  missile.y = player.y;
  if((620-missile.y+lander.y)<(missile.y-lander.y)){
    missile.direction = 4;
  } else {
    missile.direction = 6;
  };
  missile.speed = 4;

  return missile;
}
