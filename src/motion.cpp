#include <cstdlib>

#include "game_object.h"
#include "gravitize.h"
#include "object_out.h"

game_object change_elevation(game_object object){

  if(rand()%100<=5){  

    int adjust = 0;

    if(rand()%100<=50){
      adjust++;
    } else {
      adjust--;
    };
    
    if((object.x>1)&&(object.x<17)){
      object.x = object.x+adjust;        
    };
  };

  return object;
}

game_object boss_motion(game_object object, const game_object& player){
  if(((player.ytrail[7]<object.y)&&(abs(player.ytrail[7]-object.y)<310))||((player.ytrail[7]>object.y)&&(abs(player.ytrail[7]-object.y)>310))){ 
    if(player.xtrail[7]<object.x){ object.direction = 7; };
    if(player.xtrail[7]==object.x){ object.direction = 4; };
    if(player.xtrail[7]>object.x){ object.direction = 1; };
  };
  if(((player.ytrail[7]>object.y)&&(abs(player.ytrail[7]-object.y)<310))||((player.ytrail[7]<object.y)&&(abs(player.ytrail[7]-object.y)>310))){ 
    if(player.xtrail[7]<object.x){ object.direction = 9; };
    if(player.xtrail[7]==object.x){ object.direction = 6; };
    if(player.xtrail[7]>object.x){ object.direction = 3; };
  };

  return object;
}

game_object crazy_motion(game_object object, const game_object& player){
  if(((player.ytrail[9]<object.y)&&(abs(player.ytrail[9]-object.y)<310))||((player.ytrail[9]>object.y)&&(abs(player.ytrail[9]-object.y)>310))){ 
    if(player.xtrail[9]<object.x){ object.direction = 7; };
    if(player.xtrail[9]==object.x){ object.direction = 4; };
    if(player.xtrail[9]>object.x){ object.direction = 1; };
  };
  if(((player.ytrail[9]>object.y)&&(abs(player.ytrail[9]-object.y)<310))||((player.ytrail[9]<object.y)&&(abs(player.ytrail[9]-object.y)>310))){ 
    if(player.xtrail[9]<object.x){ object.direction = 9; };
    if(player.xtrail[9]==object.x){ object.direction = 6; };
    if(player.xtrail[9]>object.x){ object.direction = 3; };
  };

  return object;
}

game_object missile_motion(game_object object, const game_object& lander){
  if(((lander.y<object.y)&&(abs(lander.y-object.y)<310))||((lander.y>object.y)&&(abs(lander.y-object.y)>310))){  
    if(lander.x<object.x){ object.x-- ; };
    if(lander.x==object.x){ object.direction = 4; };
    if(lander.x>object.x){ object.x++; };
  };
  if(((lander.y>object.y)&&(abs(lander.y-object.y)<310))||((lander.y<object.y)&&(abs(lander.y-object.y)>310))){  
    if(lander.x<object.x){ object.x--; };
    if(lander.x==object.x){ object.direction = 6; };
    if(lander.x>object.x){ object.x++; };
  };

  return object;
}

game_object process_motion(game_object object, const game_object& player){

  extern int pod_in, drawlocation;  
  
  if((object.number>=2)&&(object.number<=9)&&(object.number==pod_in)){
      object.direction=player.direction;
      object.speed=player.speed;
      object.x=player.x;
      object.y=player.y;
  } else if(object.number>=110&&object.number<=121){
    object.direction = player.direction;
    object.speed = player.speed;
    if(object.number==110){
      object.x = player.x-1;
      object.y = player.y+1;
    };
    if(object.number==111){
      object.x = player.x-1;
      object.y = player.y+2;
    };
    if(object.number==112){
      object.x = player.x-1;
      object.y = player.y+3;
    };
    if(object.number==113){
      object.x = player.x+1;
      object.y = player.y+1;
    };
    if(object.number==114){
      object.x = player.x+1;
      object.y = player.y+2;
    };
    if(object.number==115){
      object.x = player.x+1;
      object.y = player.y+3;
    };
    if(object.number==116){
      object.x = player.x-1;
      object.y = player.y;
    };
    if(object.number==117){
      object.x = player.x+1;
      object.y = player.y+4;
    };
    if(object.number==118){
      object.x = player.x-1;
      object.y = player.y+4;
    };
    if(object.number==119){
      object.x = player.x+1;
      object.y = player.y;
    };
    if(object.number==120){
      object.x = player.x;
      object.y = player.y+5;
    };
    if(object.number==121){
      object.x = player.x;
      object.y = player.y-1;
    };
  } else {
    if(object.number==1){
      object.xtrail[9]=object.xtrail[8];
      object.xtrail[8]=object.xtrail[7];
      object.xtrail[7]=object.xtrail[6];
      object.xtrail[6]=object.xtrail[5];
      object.xtrail[5]=object.xtrail[4];
      object.xtrail[4]=object.xtrail[3];
      object.xtrail[3]=object.xtrail[2];
      object.xtrail[2]=object.xtrail[1];
      object.xtrail[1]=object.xtrail[0];
      object.xtrail[0]=object.x;
      object.ytrail[9]=object.ytrail[8];
      object.ytrail[8]=object.ytrail[7];
      object.ytrail[7]=object.ytrail[6];
      object.ytrail[6]=object.ytrail[5];
      object.ytrail[5]=object.ytrail[4];
      object.ytrail[4]=object.ytrail[3];
      object.ytrail[3]=object.ytrail[2];
      object.ytrail[2]=object.ytrail[1];
      object.ytrail[1]=object.ytrail[0];
      object.ytrail[0]=object.y;
    };

    if(object.direction==9){
      object.y = object.y+object.speed;
      object.x = object.x-object.speed;
    };
    if(object.direction==8){
      object.x = object.x-object.speed;
    };
    if(object.direction==7){
      object.y = object.y-object.speed;
      object.x = object.x-object.speed;
    };
    if(object.direction==6){
      object.y = object.y+object.speed;
    };
    if(object.direction==4){
      object.y = object.y-object.speed;
    };
    if(object.direction==3){
      object.y = object.y+object.speed;
      object.x = object.x+object.speed;
    };
    if(object.direction==2){
      object.x = object.x+object.speed;
    };
    if(object.direction==1){
      object.y = object.y-object.speed;
      object.x = object.x+object.speed;
    };
    
    object = gravitize(object);

    //bounds check
    if(object.x<=0){
      if(object.number>=80&&object.number<=89){ 
	object_out(object); 
      } else {
	object.x=1;
	object.direction=5;
      };
    };
    
    if(object.x>=19){
      if(object.number>=80&&object.number<=89){ 
	object_out(object); 
      } else {
        if(object.vspeed>2){
          object_out(object);
        } else {
	  object.x=18;
	  object.direction=5;
	  object.vspeed=0;
	  object.vtime=0;
	};  
      };
    };
    
    //wrap check
    if(object.y<=0){
      object.y=object.y+620;
      if(object.number==1){
	drawlocation=drawlocation+620;
      };
    };
    if(object.y>=620){
      object.y=object.y-620;
      if(object.number==1){
	drawlocation=drawlocation-620;
      };
    };
    
    if((object.number>=2)&&(object.number<=9)){
      if((object.face>=0)&&(object.face<3)){
	object.face++;
      }else{
	object.face=0;
      };
      if(object.active==1){
      //possible pod out of bounds fix hack
      if(object.x>18){object.x=18;};
      if(object.x<2){object.x=2;};
      };
    };

    if((object.number>=11)&&(object.number<=13)){
      if((object.face>=5)&&(object.face<6)){
	object.face++;
      }else{
	object.face=5;
      };
    };
  
  };
  return object;

}

game_object process_direction(game_object object, int input){
  if(input==65){input=8;};
  if(input==67){input=6;};
  if(input==68){input=4;};
  if(input==66){input=2;};
  if(input==69){input=5;};
  if(input==object.direction){
    object.speed++;
  } else {
 
    if(input==4||input==6||input==1||input==7||input==3||input==9){
      object.speed = 0;
      object.direction = input;
      
    };
    if(input==2){
      object.x++;
      if(object.vspeed<4&&object.vspeed>0){
	object.vspeed++;
      };
    };
    if(input==8){
      object.x--;
      if(object.vspeed>0){
	object.vspeed--;
	object.vtime=0;
      };
    };
  };
  
  if(input==4||input==1||input==7){object.face=0;};
  if(input==6||input==3||input==9){object.face=1;};
  if(object.speed>4){object.speed=4;};
  return object;

}

game_object pod_chase(game_object lander, const game_object& pod){
  
  if(pod.active==0){
    lander.chase=-1;
    lander.phase=0;
    lander.speed=1;
    //strcpy(lander.radar, "*"); //debug
  } else {
    
    //match y, then direction and speed
    if(lander.chase>=0&&lander.phase==1){
      if(abs(lander.y-pod.y)<3){
	lander.speed = 2;
      };
      if(lander.y==pod.y){
	lander.direction = pod.direction;
	lander.speed = pod.speed;
	lander.phase = 2;
      };
    };
    
    if(lander.chase>=0&&lander.phase==2){
      //decend to pod's x-1
      if(lander.x<=pod.x-1){
	lander.x++;
      };
      if(lander.x>pod.x-1){
	lander.phase = 3;
	lander.vtime = 0;
	lander.speed = 0;
	lander.direction = 5;
      };
    };
    
    if(lander.chase>=0&&lander.phase==3){
      //ascend at speed 1 keep pod at x+1
      if(lander.vtime==0){
	lander.x--;
      };
      if(lander.x<=1){
	//at x=1, destroy pod, deallocate lander, allocate crazy at same location.
	lander.phase = 4;  //set pod to 'kill'
      };
    };
    
  };
  return lander;
}
