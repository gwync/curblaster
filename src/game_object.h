class game_object {
  public:
    int number; //object number
    int active; //object active, 1 or 0
    int x; // x vert position
    int y; // y horiz position
    int direction; //direction, 10-key numbers 2-4-6-8 default to 5, stationary
    int face; // facing 0 left or 1 right, 4 is two-line
    int speed; //speed
    int vspeed; //fall speed
    int vtime;
    int xtrail[10]; //x history
    int ytrail[10]; //y history
    int chase; //the pod a lander is targeting
    int phase; //chase phase, also used to hold boss life
    char line0[17]; //object: left
    char line1[17]; //object: right
    char line2[17]; //object: alt1
    char line3[17]; //object: alt2
    char radar[2]; //radar symbol
};