#include <cstdlib>

#include "game_object.h"

int check_collision(const game_object& object1, const game_object& object2){
  int collided = 0;
  int spacing = 2;
  int vspacing = 0;
  if((object1.number>=100&&object1.number<=111)||(object2.number>=100&&object2.number<=111)){
    spacing = 4;
    vspacing = 2;
  };

  if(abs(object1.x-object2.x)<=vspacing){
    if(abs(object1.y-object2.y)<=spacing){
      collided=1;
    };
  };

  return collided;
};
