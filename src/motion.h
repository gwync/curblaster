game_object change_elevation(game_object);
game_object boss_motion(game_object, const game_object&);
game_object crazy_motion(game_object, const game_object&);
game_object missile_motion(game_object, const game_object&);
game_object process_motion(game_object, const game_object&);
game_object process_direction(game_object, int);
game_object pod_chase(game_object, const game_object&);
