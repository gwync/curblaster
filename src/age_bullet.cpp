#include "game_object.h"

game_object age_bullet(game_object object){
  if(object.vtime>=80){
    object.active = 0;
    object.x = 0;
    object.y = 0;
    object.speed = 0;
    object.direction = 0;
    object.vtime = 1;
  } else {
    object.vtime++;
  };

  return object;
};