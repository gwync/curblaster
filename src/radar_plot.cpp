#include <ncurses.h>
#include <cstdlib>

#include "game_object.h"

void radar_plot(game_object object, int drawlocation){

  if(object.x>0&&object.y>0&&object.active==1){
    int line = 1;  //vert plot pos
    int row = 0;  //horiz plot pos
    int x_offset = 19; //offset for height
    int y_offset = 17; //offset for display
    
    int dl_radar; //main offset

    //set line based on height rules
    if(object.x>=4&&object.x<=9){
      line = 2;
    } else if(object.x>=10&&object.x<=15){
      line = 3;
    } else if(object.x>=16&&object.x<=18){
      line = 4;
    } else {line = 0;};
    
    //set longitude based on location
    div_t quotrem;
    quotrem = div(object.y, 10);
    row = quotrem.quot;
    
    //take in radar draw location 
    div_t dl_quotrem;
    dl_quotrem = div(drawlocation, 10);
    dl_radar = dl_quotrem.quot;

    //correct to wrap radar
    
    row = row-dl_radar+25;
    
    if(row>62){row = row-62;};
    if(row<0){row = row+62;};
    
    //add offsets;

    line = line + x_offset;
    row = row + y_offset;

    //plot with symbol
    mvprintw(line,row,"%s",object.radar);
  };
}
