game_object pod_init(game_object);
game_object lander_init(game_object);
game_object crawler_init(game_object);
game_object boss_init(game_object, int);
game_object player_init(game_object);
game_object missile_init(game_object, const game_object&, const game_object&);
