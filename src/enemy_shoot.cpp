#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

#include "game_object.h"
#include "play_sound_effect.h"

game_object enemy_shoot(const game_object& enemy, const game_object& player, game_object shot, int enshotchannel, Mix_Chunk *enshotsound){

  int multiplier = 1;
  play_sound_effect(enshotsound, enshotchannel);
  shot.active=1;
  //set direction
  if(player.y<enemy.y){ 
    if(player.x<enemy.x){ shot.direction = 7; };
    if(player.x==enemy.x){ shot.direction = 4; };
    if(player.x>enemy.x){ shot.direction = 1; };
    //set multipler based on direction
    multiplier = -1; 
  };
  if(player.y>=enemy.y){
    if(player.x<enemy.x){ shot.direction = 9; };
    if(player.x==enemy.x){ shot.direction = 6; };
    if(player.x>enemy.x){ shot.direction = 3; };
  };
  shot.speed = enemy.speed;
  shot.x = enemy.x;
  shot.y = enemy.y + (enemy.speed * multiplier);

  return shot;
};
