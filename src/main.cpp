/*
   Curblaster
   Copyright (C) 2025 Gwyn Ciesla

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
   
*/

#include <ncurses.h>
#include <cstring>
#include <cstdlib>
#include <time.h>
#include <cstdio>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include <vector>
#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

#include "game_object.h"
#include "check_collision.h"
#include "age_bullet.h"
#include "radar_plot.h"
#include "object_out.h"
#include "motion.h"
#include "init.h"
#include "draw.h"
#include "powerup.h"
#include "play_sound_effect.h"
#include "mishaps.h"
#include "enemy_shoot.h"

const int podmax = 8;
const int bulletmax = 10;
const int landershotmax = 10;
const int landermax = 12;
const int crawlermax = 6;
const int missilemax = 3;
const float version = 1.15;

int score;
int lives;
int level;

int drawlocation = 0;
int pod_in = 0;
int pause_game = 0;
int shieldsleft = 400;
int tripshot = 0;
int laser = 0;
int missile = 0;
int super_shield = 0;
int sound = 1;
int newgame = 0;

int main(int argc, const char *argv[]){

  //Initialize world
  
  if(argc==2&&strcmp(argv[1], "--nosound")==0){ sound = 0; };
 
  srand(time(NULL));

  level = 1;
  score = 0;
  lives = 4;

  //player
  game_object player;
  player.number = 1;
  player.active = 1;
  player.x = 8;
  player.y = 35;
  player.direction = 5;
  player.face = 1;
  player.speed = 0;
  player.vspeed = 0;
  player.vtime = 0;
  player.xtrail[0] = 0;
  player.xtrail[1] = 0;
  player.ytrail[0] = 0;
  player.ytrail[1] = 0;
  player.phase = 0;
  player.chase = -1;
  strcpy (player.line0, "<==_>");
  strcpy (player.line1, "<_==>");
  strcpy (player.radar, "=");

  //gate
  game_object gate;
  gate.number = 10;
  gate.active = 1;
  gate.x = 18;
  gate.y = 40;
  gate.direction = 5;
  gate.face = 4;
  gate.speed = 0;
  gate.vspeed = 0;
  gate.vtime = 0;
  strcpy (gate.line0, " ^ ");
  strcpy (gate.line1, "/|\\");
  strcpy (gate.radar, "|");
  gate.chase = -1;
  gate.phase = 0;
 
  game_object gatebeam1;
  gatebeam1.number = 11;
  gatebeam1.active = 1;
  gatebeam1.x = 16;
  gatebeam1.y = 40;
  gatebeam1.direction = 5;
  gatebeam1.face = 5;
  gatebeam1.speed = 0;
  gatebeam1.vspeed = 0;
  gatebeam1.vtime = 0;
  gatebeam1.phase = 0;
  gatebeam1.chase = -1;
  strcpy (gatebeam1.line0, " ] ");
  strcpy (gatebeam1.line1, " [ ");
  strcpy (gatebeam1.line2, " [ ");
  strcpy (gatebeam1.line3, " ] ");
  strcpy (gatebeam1.radar, "|");

  game_object gatebeam2;
  gatebeam2.number = 12;
  gatebeam2.active = 1;
  gatebeam2.x = 14;
  gatebeam2.y = 40;
  gatebeam2.direction = 5;
  gatebeam2.face = 5;
  gatebeam2.speed = 0;
  gatebeam2.vspeed = 0;
  gatebeam2.vtime = 0;
  gatebeam2.phase = 0;
  gatebeam2.chase = -1;
  strcpy (gatebeam2.line0, " ] ");
  strcpy (gatebeam2.line1, " [ ");
  strcpy (gatebeam2.line2, " [ ");
  strcpy (gatebeam2.line3, " ] ");
  strcpy (gatebeam2.radar, "|");

  game_object gatebeam3;
  gatebeam3.number = 13;
  gatebeam3.active = 1;
  gatebeam3.x = 12;
  gatebeam3.y = 40;
  gatebeam3.direction = 5;
  gatebeam3.face = 5;
  gatebeam3.speed = 0;
  gatebeam3.vspeed = 0;
  gatebeam3.vtime = 0;
  gatebeam3.phase = 0;
  gatebeam3.chase = -1;
  strcpy (gatebeam3.line0, " ] ");
  strcpy (gatebeam3.line1, " [ ");
  strcpy (gatebeam3.line2, " [ ");
  strcpy (gatebeam3.line3, " ] ");
  strcpy (gatebeam3.radar, "|");

  std::vector<game_object> gatebeams;
  gatebeams.insert(gatebeams.begin(), gatebeam1);
  gatebeams.insert(gatebeams.begin(), gatebeam2);
  gatebeams.insert(gatebeams.begin(), gatebeam3);

  //pods
  game_object pods[podmax] = {
    { 2, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 3, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 4, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 5, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 6, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 7, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 8, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 9, 1, 18, rand()%620, 0, 0, 1, 0, 0 }
 };

  for(int podloop = 0; podloop<podmax; podloop=podloop+2){ 
    if(rand()%1000>=500){
      pods[podloop].direction = 4;
    } else {
      pods[podloop].direction = 6;
    };

    strcpy (pods[podloop].line0, " x ");
    strcpy (pods[podloop].line1, " x ");
    strcpy (pods[podloop].line2, " + ");
    strcpy (pods[podloop].line3, " + ");
    strcpy (pods[podloop].radar, "x");

    pods[podloop].chase = -1;
  };

  for(int podloop = 1; podloop<podmax+1; podloop=podloop+2){ 
    if(rand()%1000>=500){
      pods[podloop].direction = 4;
    } else {
      pods[podloop].direction = 6;
    };
    
    strcpy (pods[podloop].line0, " + ");
    strcpy (pods[podloop].line1, " + ");
    strcpy (pods[podloop].line2, " x ");
    strcpy (pods[podloop].line3, " x ");
    strcpy (pods[podloop].radar, "x");
    
    pods[podloop].chase = -1;
  };

  //bullets
  game_object bullets[bulletmax] = {
    { 40, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 41, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 42, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 43, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 44, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 45, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 46, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 47, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 48, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 49, 0, 0, 0, 5, 0, 0, 0, 0 }
  };
  
  for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
    strcpy (bullets[bulletloop].line0, " - ");
    strcpy (bullets[bulletloop].line1, " - ");
  };

  //landers
  game_object landers[landermax] = {
    { 60, 1, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 61, 1, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 62, 1, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 63, 1, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 64, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 65, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 66, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 67, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 68, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 69, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 70, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 },
    { 71, 0, rand()%16, rand()%620, 0, 0, 1, 0, 0 }
  };
  
  for(int landerloop = 0; landerloop<landermax; landerloop++){ 
    if(rand()%1000>=500){
      landers[landerloop].direction = 4;
    } else {
      landers[landerloop].direction = 6;
    };

    landers[landerloop].chase = -1;

    strcpy (landers[landerloop].line0, "{=}");
    strcpy (landers[landerloop].radar, "*");
  };

  //lander shots
  game_object landershot[landershotmax] = {
    { 80, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 81, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 82, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 83, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 84, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 85, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 86, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 87, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 88, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 89, 0, 0, 0, 5, 0, 0, 0, 0 }
  };
  
  for(int landershotloop = 0; landershotloop<landershotmax; landershotloop++){ 
    strcpy (landershot[landershotloop].line0, " . ");
    strcpy (landershot[landershotloop].line1, " . ");
  };

//crazies
  game_object crazies[landermax] = {
    { 90, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 91, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 92, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 93, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 94, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 95, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 96, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 97, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 98, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 99, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 100, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 101, 0, 0, 0, 4, 0, 1, 0, 0 }
  };
  
  for(int crazyloop = 0; crazyloop<landermax; crazyloop++){ 
    strcpy (crazies[crazyloop].line0, "}!{");
    strcpy (crazies[crazyloop].radar, "!");
  };

  //shield
  game_object shields[12] = {
    { 110, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 111, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 112, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 113, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 114, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 115, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 116, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 117, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 118, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 119, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 120, 1, 0, 0, 5, 0, 1, 0, 0 },
    { 121, 1, 0, 0, 5, 0, 1, 0, 0 }
  };

  strcpy (shields[0].line0, "-");
  strcpy (shields[1].line0, "-");
  strcpy (shields[2].line0, "-");
  strcpy (shields[3].line0, "-");
  strcpy (shields[4].line0, "-");
  strcpy (shields[5].line0, "-");
  strcpy (shields[6].line0, "/");
  strcpy (shields[7].line0, "/");
  strcpy (shields[8].line0, "\\");
  strcpy (shields[9].line0, "\\");
  strcpy (shields[10].line0, ")");
  strcpy (shields[11].line0, "(");

  game_object powerups[4] = {
    { 130, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 131, 0, 0, 0, 4, 0, 1, 0, 0 },
    { 132, 0, 0, 0, 6, 0, 1, 0, 0 },
    { 133, 0, 0, 0, 4, 0, 1, 0, 0 }
  };

  //Reference loops 200-230
  game_object refs[31] = {
    { 200, 1, 19, 0, 5, 0, 0, 0, 0 },
    { 201, 1, 19, 20, 5, 0, 0, 0, 0 },
    { 202, 1, 19, 40, 5, 0, 0, 0, 0 },
    { 203, 1, 19, 60, 5, 0, 0, 0, 0 },
    { 204, 1, 19, 80, 5, 0, 0, 0, 0 },
    { 205, 1, 19, 100, 5, 0, 0, 0, 0 },
    { 206, 1, 19, 120, 5, 0, 0, 0, 0 },
    { 207, 1, 19, 140, 5, 0, 0, 0, 0 },
    { 208, 1, 19, 160, 5, 0, 0, 0, 0 },
    { 209, 1, 19, 180, 5, 0, 0, 0, 0 },
    { 210, 1, 19, 200, 5, 0, 0, 0, 0 },
    { 211, 1, 19, 220, 5, 0, 0, 0, 0 },
    { 212, 1, 19, 240, 5, 0, 0, 0, 0 },
    { 213, 1, 19, 260, 5, 0, 0, 0, 0 },
    { 214, 1, 19, 280, 5, 0, 0, 0, 0 },
    { 215, 1, 19, 300, 5, 0, 0, 0, 0 },
    { 216, 1, 19, 320, 5, 0, 0, 0, 0 },
    { 217, 1, 19, 340, 5, 0, 0, 0, 0 },
    { 218, 1, 19, 360, 5, 0, 0, 0, 0 },
    { 219, 1, 19, 380, 5, 0, 0, 0, 0 },
    { 220, 1, 19, 400, 5, 0, 0, 0, 0 },
    { 221, 1, 19, 420, 5, 0, 0, 0, 0 },
    { 222, 1, 19, 440, 5, 0, 0, 0, 0 },
    { 223, 1, 19, 460, 5, 0, 0, 0, 0 },
    { 224, 1, 19, 480, 5, 0, 0, 0, 0 },
    { 225, 1, 19, 500, 5, 0, 0, 0, 0 },
    { 226, 1, 19, 520, 5, 0, 0, 0, 0 },
    { 227, 1, 19, 540, 5, 0, 0, 0, 0 },
    { 228, 1, 19, 560, 5, 0, 0, 0, 0 },
    { 229, 1, 19, 580, 5, 0, 0, 0, 0 },
    { 230, 1, 19, 600, 5, 0, 0, 0, 0 }
  };

  for(int refloop = 0; refloop<31; refloop++){ 
    strcpy (refs[refloop].line0, "|");
    strcpy (refs[refloop].line1, "|");
  };

  //crawlers 
  game_object crawlers[crawlermax] = {
    { 30, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 31, 1, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 32, 0, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 33, 0, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 34, 0, 18, rand()%620, 0, 0, 1, 0, 0 },
    { 35, 0, 18, rand()%620, 0, 0, 1, 0, 0 }
  };
  
  for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){ 
    if(rand()%1000>=500){
      crawlers[crawlerloop].direction = 4;
    } else {
      crawlers[crawlerloop].direction = 6;
    };

    strcpy (crawlers[crawlerloop].line0, "/=\\");
    strcpy (crawlers[crawlerloop].radar, "^");
  };

  game_object bosses[6] = {
    { 300, 0, 0, 0, 5, 4, 0, 0, 0 },
    { 301, 0, 0, 0, 5, 4, 0, 0, 0 },
    { 302, 0, 0, 0, 5, 4, 0, 0, 0 },
    { 303, 0, 0, 0, 5, 4, 0, 0, 0 },
    { 304, 0, 0, 0, 5, 4, 0, 0, 0 },
    { 305, 0, 0, 0, 5, 4, 0, 0, 0 }
  };

  strcpy (bosses[0].line0, "");  
  strcpy (bosses[0].line1, "/==O==\\");
  strcpy (bosses[0].radar, "O");

  strcpy (bosses[1].line0, "   /=O=\\");
  strcpy (bosses[1].line1, "/===+=+===\\");
  strcpy (bosses[1].radar, "O");
  
  strcpy (bosses[2].line0, "      /O\\");
  strcpy (bosses[2].line1, "<==/==[=]==\\==>");
  strcpy (bosses[2].radar, "O");
  
  strcpy (bosses[3].line0, "/o-o\\");
  strcpy (bosses[3].line1, "\\|_|/");
  strcpy (bosses[3].radar, "O");
  
  strcpy (bosses[4].line0, "(----)");
  strcpy (bosses[4].line1, " /||\\");
  strcpy (bosses[4].radar, "O");
  
  strcpy (bosses[5].line0, "");
  strcpy (bosses[5].line1, ".");
  strcpy (bosses[5].radar, "O");

  //boomstuff
  game_object boomstuff;
  boomstuff.number = 400;
  boomstuff.active = 0;
  boomstuff.x = 0;
  boomstuff.y = 0;
  boomstuff.direction = 5;
  boomstuff.face = 1;
  boomstuff.speed = 0; 
  boomstuff.vspeed = 0;
  boomstuff.vtime = 0;
  boomstuff.phase = 0; 
  boomstuff.chase = -1;
  strcpy (boomstuff.line0, "\\");
  strcpy (boomstuff.line1, "/");

  //missiles
  game_object missiles[missilemax] = {
    { 50, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 51, 0, 0, 0, 5, 0, 0, 0, 0 },
    { 52, 0, 0, 0, 5, 0, 0, 0, 0 }
  };

  for(int missileloop = 0; missileloop<missilemax; missileloop++){ 
    strcpy (missiles[missileloop].line0, "---");
    strcpy (missiles[missileloop].line1, "---");
    strcpy (missiles[missileloop].radar, "-");
    
    missiles[missileloop].chase = -1;
  };

  initscr();

  //Check screen size 80x24 and exit if not big enough
  int maxx, maxy;

  getmaxyx(stdscr, maxx, maxy);

  if(maxx<24||maxy<80){
    endwin();
    printf("Screen only %d x %d, needs to be at least 80x24\n", maxy, maxx);
    exit(1);
  };

  //main loop

  int loopvar = 0;
  int podcount = 0;
  int podsin = 0;
  int landercount = 0;
  int crazycount;
  int crawlercount;
  int bosscount;
  int alertleft = 0;
  int alertright = 0;
  int smartbombs = 2;
  int shieldup = 0;
  int drawlaser = 0;

  cbreak();
  halfdelay(1);
  noecho();

  //print title screen

  //border
  mvprintw(0,0,"--------------------------------------------------------------------------------\n");
  mvprintw(23,0,"--------------------------------------------------------------------------------\n");
  for(int a=1;a<=22;a++){ mvprintw(a,0,"|"); mvprintw(a,79,"|"); };

  //title
   mvprintw(4,12,"+------");
   mvprintw(5,12,"|                    |     |");
   mvprintw(6,12,"|                    |     |");           
   mvprintw(7,12,"|                    |     |");
   mvprintw(8,12,"|              |     +--+  |   --+  +---   |     +---+  |");
   mvprintw(9,12,"|        |  |  +--+  |  |  |     |  |    +-+-++  |  /   +--+");
  mvprintw(10,12,"|        |  |  |  |  |  |  |  +--+  +--+   |     +-/    |  |");
  mvprintw(11,12,"+------  +--+  |     +--+  |  +--+  ---+   |     +---   |");
  
  //Version
  mvprintw(12,34,"Version %1.2f",version);

  //Copyright
  mvprintw(14,22,"Copyright (C) 2025 Gwyn Ciesla");

  //player, lander, pods, gate
  mvprintw(17,5,"<_==>     -   - - - ");
  mvprintw(19,62,"{=}");
  mvprintw(16,56,"{=}");
  mvprintw(20,63,"x");
  mvprintw(22,15,"+                     x");
  mvprintw(22,69,"/|\\");
  mvprintw(21,70,"^");
  mvprintw(20,70,"]");
  mvprintw(19,70,"[");
  mvprintw(18,70,"]");
  mvprintw(17,70,"[");
  mvprintw(16,70,"]");
  mvprintw(15,70,"[");

  //high score

  int hscore = 0;
  char filename[250];
  strcpy (filename, "/.curblaster_scores");
  char home[250];
  strncpy(home, getenv("HOME"), 230);
  
  strncat(home, filename, 20);

  FILE *scorefile = fopen(home, "r");
  if(scorefile != NULL){
    char workchara[6];
    if(fgets(workchara, 6, scorefile) != NULL){
      hscore = atoi(workchara);
    };
    fclose(scorefile);
  } else {
    FILE *writescorefile = fopen(home, "w");
    if(writescorefile != NULL){
      fputs("0\n", writescorefile);
      fclose(writescorefile);
      hscore = 0;
    };
  };
  
  mvprintw(13,34,"High Score: %d",hscore);
  
  //get the cursor out of the way
  mvprintw(23,79,"-");

  //load sound, music
  Mix_Chunk *shotsound = NULL;
  int shotchannel = -1;
  Mix_Chunk *boomsound = NULL;
  int boomchannel = -1;
  Mix_Chunk *dropoffsound = NULL;
  int dropoffchannel = -1;
  Mix_Chunk *pickupsound = NULL;
  int pickupchannel = -1;
  Mix_Chunk *lasersound = NULL;
  int laserchannel = -1;
  Mix_Chunk *warningsound = NULL;
  int warningchannel = -1;
  Mix_Chunk *pupcreatesound = NULL;
  int pupcreatechannel = -1;
  Mix_Chunk *puptakesound = NULL;
  int puptakechannel = -1;
  Mix_Chunk *shieldupsound = NULL;
  int shieldupchannel = -1;
  Mix_Chunk *shielddownsound = NULL;
  int shielddownchannel = -1;
  Mix_Chunk *crazifysound = NULL;
  int crazifychannel = -1;
  Mix_Chunk *enshotsound = NULL;
  int enshotchannel = -1;    
  Mix_Chunk *missilesound = NULL;
  int missilechannel = -1;        
  Mix_Chunk *levelendsound = NULL;
  int levelendchannel = -1;        

  Mix_Music *title = NULL;

  const char *env_tty;
  env_tty = getenv("SSH_TTY");

  struct stat datatest;
    
  if(env_tty == NULL && sound == 1){

    int audio_rate = 22050;
    Uint16 audio_format = AUDIO_S16;
    int audio_channels = 6;
    int audio_buffers = 4096;


    SDL_Init(SDL_INIT_AUDIO);

    if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
      printf("Unable to open audio!\n");
      exit(1);
    }

    std::string path_prefix;

    if(stat("data/boom.ogg", &datatest)==0) {
      path_prefix = "";
    } else if(stat("/usr/local/share/curblaster/data/boom.ogg", &datatest)==0) {
      path_prefix = "/usr/local/share/curblaster/";
    } else {
      path_prefix = "/usr/share/curblaster/";
    }
    shotsound = Mix_LoadWAV((path_prefix + "data/shot.ogg").c_str());
    boomsound = Mix_LoadWAV((path_prefix + "data/boom.ogg").c_str());
    dropoffsound = Mix_LoadWAV((path_prefix + "data/dropoff.ogg").c_str());
    pickupsound = Mix_LoadWAV((path_prefix + "data/pickup.ogg").c_str());
    lasersound = Mix_LoadWAV((path_prefix + "data/laser.ogg").c_str());
    warningsound = Mix_LoadWAV((path_prefix + "data/warning.ogg").c_str());
    pupcreatesound = Mix_LoadWAV((path_prefix + "data/pup-create.ogg").c_str());
    puptakesound = Mix_LoadWAV((path_prefix + "data/pup-take.ogg").c_str());
    shieldupsound = Mix_LoadWAV((path_prefix + "data/shieldup.ogg").c_str());
    shielddownsound = Mix_LoadWAV((path_prefix + "data/shielddown.ogg").c_str());
    crazifysound = Mix_LoadWAV((path_prefix + "data/crazify.ogg").c_str());
    enshotsound = Mix_LoadWAV((path_prefix + "data/enshot.ogg").c_str());
    missilesound = Mix_LoadWAV((path_prefix + "data/missile.ogg").c_str());
    levelendsound = Mix_LoadWAV((path_prefix + "data/level_end.ogg").c_str());
    title = Mix_LoadMUS((path_prefix + "data/title_music.ogg").c_str());
  } else {
    sound = 0;
  }

  //start title music
  Mix_PlayMusic(title, -1);

  //hold until SPACE pressed
  pause_game = 0;
  int counter = 0;
  int show_controls = 30;
  while(pause_game!=' '){
    //cbreak();
    pause_game = getch();
    if(counter>=4){
      //GO!
      mvprintw(18,28,"Press SPACE to start");
      mvprintw(23,79,"-");
      if(counter>=8){
	counter=0;
      };
    } else {
      mvprintw(18,28,"                    ");
      mvprintw(23,79,"-");
    };
    counter++;
    //halfdelay(1);
  };
  
  Mix_HaltMusic();
  Mix_FreeMusic(title);
  title = NULL;
  
  while(loopvar == 0){

    int input = 0;
    
    clear();
    // Draw board
    draw_board(score, lives, level, shieldsleft, missile);

    //process object motion
    player = process_motion(player, player);
    
    for(int shieldloop = 0; shieldloop<12; shieldloop++){
      shields[shieldloop] = process_motion(shields[shieldloop], player);
    };

    for(int poweruploop = 0; poweruploop<4; poweruploop++){
      powerups[poweruploop] = process_motion(powerups[poweruploop], player);
    };

    for(int podloop = 0; podloop<podmax; podloop++){ 
      pods[podloop] = process_motion(pods[podloop], player); 
      //If lander dies, release pod
      if(pods[podloop].chase>=0){  
	for(int landerloop = 0; landerloop<landermax; landerloop++){ 
	  if(landerloop==pods[podloop].chase){
	    if(landers[landerloop].active==0){
	      pods[podloop].vspeed = 2;
	      pods[podloop].vtime = 1;
	      pods[podloop].chase = -1;
	      pods[podloop].speed = 1;
	      if(rand()%1000>=500){
		pods[podloop].direction = 4;
	      } else {
		pods[podloop].direction = 6;
	      };
	    };
	  };
	};
      };
    };

    for (std::vector<game_object>::iterator it=gatebeams.begin(); it<gatebeams.end(); ++it){
      *it = process_motion(*it, player);
    };
    
    for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
      if(bullets[bulletloop].active==1){ 
	bullets[bulletloop] = process_motion(bullets[bulletloop], player);
	bullets[bulletloop] = age_bullet(bullets[bulletloop]);
      };
    };
    
    for(int missileloop = 0; missileloop<missilemax; missileloop++){ 
      if(missiles[missileloop].active==1){ 
        if(landers[missiles[missileloop].chase].active==1){
          missiles[missileloop] = missile_motion(missiles[missileloop], landers[missiles[missileloop].chase]);
	  missiles[missileloop] = process_motion(missiles[missileloop], player);
	  //missiles[missileloop] = age_bullet(missiles[missileloop]);  //Something to think about
        } else {
	  drawlocation = boom_object(drawlocation, boomstuff, missiles[missileloop], boomsound, boomchannel);
	  missiles[missileloop] = object_out(missiles[missileloop]);
        };
      };
    };

    for(int landerloop = 0; landerloop<landermax; landerloop++){ 
      if(landers[landerloop].active==1){ 
	if(landers[landerloop].chase>=0&&landers[landerloop].phase==1){
	  landers[landerloop] = pod_chase(landers[landerloop], pods[landers[landerloop].chase]); 
	};
	if(landers[landerloop].chase>=0&&landers[landerloop].phase==2){
	  landers[landerloop] = pod_chase(landers[landerloop], pods[landers[landerloop].chase]); 
	};
	if(landers[landerloop].chase>=0&&landers[landerloop].phase==3){
	  pods[landers[landerloop].chase].speed = 0;
	  pods[landers[landerloop].chase].direction = 5;
	  if(landers[landerloop].vtime<=10){
	    landers[landerloop].vtime++;
	  } else { 
	    pods[landers[landerloop].chase].x--;
	    landers[landerloop].vtime=0;
	  };
	  landers[landerloop] = pod_chase(landers[landerloop], pods[landers[landerloop].chase]); 
	};
	if(landers[landerloop].chase>=0&&landers[landerloop].phase==4){ //decode which pod to kill
	  pods[landers[landerloop].chase] = object_out(pods[landers[landerloop].chase]);
	  crazies[landerloop] = encrazify(landers[landerloop], crazies[landerloop], crazifysound, crazifychannel);
	  landers[landerloop] = object_out(landers[landerloop]);
	};
      };
      if(landers[landerloop].chase<=0&&landers[landerloop].phase<=2){ 
        landers[landerloop] = change_elevation(landers[landerloop]);
      };
      landers[landerloop] = process_motion(landers[landerloop], player);
    };
    
    
    for(int crazyloop = 0; crazyloop<landermax; crazyloop++){ 
      if(crazies[crazyloop].active==1){ 
	crazies[crazyloop] = crazy_motion(crazies[crazyloop], player);
	crazies[crazyloop] = process_motion(crazies[crazyloop], player);
      };
    };

    for(int landershotloop = 0; landershotloop<landershotmax; landershotloop++){ 
      if(landershot[landershotloop].active==1){ 
	landershot[landershotloop] = process_motion(landershot[landershotloop], player);
	landershot[landershotloop] = age_bullet(landershot[landershotloop]);
      };
    };

    for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
      crawlers[crawlerloop] = process_motion(crawlers[crawlerloop], player);
    };

    for(int bossloop = 0; bossloop<6; bossloop++){
      if(bosses[bossloop].active==1){
        bosses[bossloop] = boss_motion(bosses[bossloop], player);
        bosses[bossloop] = process_motion(bosses[bossloop], player);
      };
    };

    // Draw player object 1
    drawlocation = draw_object(player, drawlocation);

    if(shieldup==1){
      for(int shieldloop = 0; shieldloop<12; shieldloop++){ 
      	drawlocation = draw_object(shields[shieldloop], drawlocation);
      };
    };

    for(int poweruploop = 0; poweruploop<4; poweruploop++){
      drawlocation = draw_object(powerups[poweruploop], drawlocation);
    };

    // Draw reference points 200-230
    for(int refloop = 0; refloop<31; refloop++){
      drawlocation = draw_object(refs[refloop], drawlocation);
    };

    // Draw pods objects 2-9    
    for(int podloop = 0; podloop<podmax; podloop++){
      if(pods[podloop].number!=pod_in){
        drawlocation = draw_object(pods[podloop], drawlocation); 
      };  
    };

    // Draw gate object 10
    drawlocation = draw_object(gate, drawlocation);
    for (std::vector<game_object>::iterator it=gatebeams.begin(); it<gatebeams.end(); ++it){
      drawlocation = draw_object(*it, drawlocation); // cppcheck-suppress useStlAlgorithm
    };

    // Draw landers objects 60-71
    for(int landerloop = 0; landerloop<landermax; landerloop++){ 
      if(landers[landerloop].active==1){ 
	drawlocation = draw_object(landers[landerloop], drawlocation);
      };
    };
    
    // Draw crazies objects 90-101
    for(int crazyloop = 0; crazyloop<landermax; crazyloop++){ 
      if(crazies[crazyloop].active==1){ 
	drawlocation = draw_object(crazies[crazyloop], drawlocation);
      };
    };

    // Draw shots objects 40-43
    for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
      if(bullets[bulletloop].active==1){ 
	drawlocation = draw_object(bullets[bulletloop], drawlocation);
      };
    };
    
    // Draw missiles objects 50-53
    for(int missileloop = 0; missileloop<missilemax; missileloop++){ 
      if(missiles[missileloop].active==1){ 
	drawlocation = draw_object(missiles[missileloop], drawlocation);
      };
    };
    
    //draw laser beam
    if(drawlaser==1){
      
      int offset = 0;
      
      offset = player.y-drawlocation;
      
      if(drawlocation>=540&&player.y<620-drawlocation){
	offset = 620-drawlocation+player.y;  //right of player wrap correction
      };
      
      if(drawlocation<0&&player.y>=540){
	offset = abs(drawlocation)-abs(620-player.y);  //left of player wrap correction
      };

      if(player.face == 0){
	//draw
	for(int x=offset-1;x>=0;x--){
	  mvprintw(player.x,x,"-");
	};
	
      };
      if(player.face == 1){
	//draw
	for(int x=offset+5;x<=79;x++){
	  mvprintw(player.x,x,"-");
	};
      };
      drawlaser=0;
    };

      // Draw enemy shots 80-83
    for(int landershotloop = 0; landershotloop<landershotmax; landershotloop++){ 
      if(landershot[landershotloop].active==1){ 
	drawlocation = draw_object(landershot[landershotloop], drawlocation);
      };
    };

    // Draw crawlers 30-35
    for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
      drawlocation = draw_object(crawlers[crawlerloop], drawlocation); 
    };

    // Draw bosses 300-305
    for(int bossloop = 0; bossloop<6; bossloop++){
      drawlocation = draw_object(bosses[bossloop], drawlocation); 
    };

    //Radar plot 18x620 represented in 4x62
    radar_plot(gate, drawlocation);
    for (std::vector<game_object>::iterator it=gatebeams.begin(); it<gatebeams.end(); ++it){
      radar_plot(*it, drawlocation);
    };
    for(int podloop = 0; podloop<podmax; podloop++){ radar_plot(pods[podloop], drawlocation); };
    for(int landerloop = 0; landerloop<landermax; landerloop++){ radar_plot(landers[landerloop], drawlocation); };
    for(int crazyloop = 0; crazyloop<landermax; crazyloop++){ radar_plot(crazies[crazyloop], drawlocation); };
    for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){ radar_plot(crawlers[crawlerloop], drawlocation); };
    for(int bossloop = 0; bossloop<6; bossloop++){ radar_plot(bosses[bossloop], drawlocation); };
    for(int missileloop = 0; missileloop<missilemax; missileloop++){ radar_plot(missiles[missileloop], drawlocation); };
    radar_plot(player, drawlocation);

    //alerts
    if(alertright>0){
      if(alertright%2==0){
        warningchannel = play_sound_effect(warningsound, warningchannel);
	mvprintw(19,50," >> ");
      };
      alertright--;
    };
    if(alertleft>0){
      if(alertleft%2==0){
        warningchannel = play_sound_effect(warningsound, warningchannel);
	mvprintw(19,40," << ");
      };
      alertleft--;
    };
    
    //pod state
    mvprintw(21,1,"O:%d I:%d", podcount, podsin);

    //smartbombs
    int count = smartbombs;
    for(int x=9;count>0;x++){
      mvprintw(21,x,"B");
      count--;
    };

    if(show_controls>=1){
      display_controls();
      show_controls--;
    };

    //get the cursor out of the way
    mvprintw(23,79,"|");


    input = getch();
    
    //quit
    if(input=='q'){
    cbreak();
    clear();
    printw("Really quit? (y/n)");
    char quit = getch();
    if(quit=='y'){
      printw("\nBye Bye!\n");
      loopvar=1;       
    };
    halfdelay(1);
    };

    //check for/toggle shield
    if(input=='s'){
      if(shieldup==0){
	if(shieldsleft>=1){
	  shieldup=1;
	  shieldupchannel = play_sound_effect(shieldupsound, shieldupchannel);
	};
      } else {
	shieldup=0;
	shielddownchannel = play_sound_effect(shielddownsound, shielddownchannel);
      };
    };

    if(shieldup==1&&super_shield>0){
      for(int shieldloop = 0; shieldloop<12; shieldloop++){
        if(shields[shieldloop].phase==0){
          shields[shieldloop].phase=1;
          super_shield--;
        } else {
          shields[shieldloop].phase=0;
        };
      };
    };

    //deplete shields if necessary.
    if(shieldup==1){
      if(shieldsleft>0){
	shieldsleft--;
      } else {
	shieldup=0;
	shielddownchannel = play_sound_effect(shielddownsound, shielddownchannel);
      };
    };

    //check for / process direction change
    if(input==66||input==68||input==67||input==65||input==69){
      player = process_direction(player, input);
    };

    //pause_game
    if(input==' '){
      pause_game = 0;
      while(pause_game!=' '){
	cbreak();
	display_controls();
	mvprintw(10,25,"Paused.  Press SPACE to continue.\n");
	pause_game = getch();
	halfdelay(1);
      };
    };

    //check for powerup pickup
    for(int puploop = 0; puploop<4; puploop++){
      if((powerups[puploop].active==1)&&(check_collision(powerups[puploop], player)==1)){
        puptakechannel = play_sound_effect(puptakesound, puptakechannel);
	powerups[puploop] = object_out(powerups[puploop]);
	if(strncmp(powerups[puploop].line0, "T", 1)==0){
	  tripshot = tripshot + 100;
	};
	if(strncmp(powerups[puploop].line0, "S", 1)==0){
	  shieldsleft = shieldsleft + 100;
	};
	if(strncmp(powerups[puploop].line0, "K", 1)==0){
	  super_shield = super_shield + 100;
	};
	if(strncmp(powerups[puploop].line0, "L", 1)==0){
	  if(lives<4){
	    lives++;
          };
	};
	if(strncmp(powerups[puploop].line0, "B", 1)==0){
	  if(smartbombs<6){
	    smartbombs++;
          };
	};
	if(strncmp(powerups[puploop].line0, "Z", 1)==0){
	  laser = laser + 100;
	};
	if(strncmp(powerups[puploop].line0, "M", 1)==0){
	  if(missile<3){
	    missile++;
          };
	};
      };
    };
    
    //check for / process pod pickup
    for(int podloop = 0; podloop<podmax; podloop++){ 
      if((pod_in==0)&&(pods[podloop].active==1)&&(check_collision(pods[podloop], player)==1)){
        pickupchannel = play_sound_effect(pickupsound, pickupchannel);
	pod_in = pods[podloop].number;
	pods[podloop].chase = -1;
	//stop the chase if we steal the pod
	for(int landerloop = 0; landerloop<landermax; landerloop++){
	  if(landers[landerloop].chase==podloop){ 
	    landers[landerloop].chase = -1; 
	    landers[landerloop].phase = 0; 
	    score = score + 20 - player.x;
	  };
	};
	score = score + 20 - player.x;
      };
    };
    
    if(pod_in>0){
      strcpy (player.line0, "<==x>");
      strcpy (player.line1, "<x==>");
    };
    
    //if pod in hand, check for gate dropoff
    if(pod_in>0){
      if((abs(player.y-gate.y)<player.speed)&&(player.x>10)){
        dropoffchannel = play_sound_effect(dropoffsound, dropoffchannel);
	//set pod inactive
	for(int podloop = 0; podloop<podmax; podloop++){ if(pod_in==pods[podloop].number){ pods[podloop] = object_out(pods[podloop]); }; };
	
	//increment score
	score = score + 10;
	podsin++;
        pod_in = 0;
	strcpy (player.line0, "<==_>");
	strcpy (player.line1, "<_==>");
      };
    };
    //check for / process player fire
    if(input=='z'){
      //we fired
      //if laser, do that
      if(laser>0){
	drawlaser=1;
	laserchannel = play_sound_effect(lasersound, laserchannel);
	//kill landers
	for(int landerloop = 0; landerloop<landermax; landerloop++){
	  if(((abs(player.y-landers[landerloop].y)<=80)||(abs(player.y-landers[landerloop].y)>=540))&&(player.x==landers[landerloop].x)&&((player.face==0&&(landers[landerloop].y<player.y))||(player.face==1&&(landers[landerloop].y>player.y)))){
	    const game_object power_obj = landers[landerloop];
	    determine_powerup(powerups, power_obj, 800, pupcreatesound, pupcreatechannel);
	    drawlocation = boom_object(drawlocation, boomstuff, landers[landerloop], boomsound, boomchannel);
	    landers[landerloop] = object_out(landers[landerloop]);
	    score = score + 15;
	  };
	};
	//kill crazies
	for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
	  if(((abs(player.y-crazies[crazyloop].y)<=80)||(abs(player.y-crazies[crazyloop].y)>=540))&&(player.x==crazies[crazyloop].x)&&((player.face==0&&(crazies[crazyloop].y<player.y))||(player.face==1&&(crazies[crazyloop].y>player.y)))){
	    const game_object power_obj = crazies[crazyloop];
	    determine_powerup(powerups, power_obj, 800, pupcreatesound, pupcreatechannel);
	    drawlocation = boom_object(drawlocation, boomstuff, crazies[crazyloop], boomsound, boomchannel);
	    crazies[crazyloop] = object_out(crazies[crazyloop]);
	    score = score + 15;
	  };
	};
	//kill crawlers
	for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
	  if(((abs(player.y-crawlers[crawlerloop].y)<=80)||(abs(player.y-crawlers[crawlerloop].y)>=540))&&(player.x==crawlers[crawlerloop].x)&&((player.face==0&&(crawlers[crawlerloop].y<player.y))||(player.face==1&&(crawlers[crawlerloop].y>player.y)))){
	    const game_object power_obj = crawlers[crawlerloop];
	    determine_powerup(powerups, power_obj, 800, pupcreatesound, pupcreatechannel);
	    drawlocation = boom_object(drawlocation, boomstuff, crawlers[crawlerloop], boomsound, boomchannel);
	    crawlers[crawlerloop] = object_out(crawlers[crawlerloop]);
	    score = score + 15;
	  };
	};
	//hurt/kill boss
	for(int bossloop = 0; bossloop<6; bossloop++){
	  if(bosses[bossloop].active==1){
	    if(((abs(player.y-bosses[bossloop].y)<=80)||(abs(player.y-bosses[bossloop].y)>=540))&&(player.x==bosses[bossloop].x)&&((player.face==0&&(bosses[bossloop].y<player.y))||(player.face==1&&(bosses[bossloop].y>player.y)))){
	      bosses[bossloop].phase = bosses[bossloop].phase - 5;
	      if(bosses[bossloop].phase<=0){
	        const game_object power_obj = bosses[bossloop];
	        determine_powerup(powerups, power_obj, 600, pupcreatesound, pupcreatechannel);
	        drawlocation = boom_object(drawlocation, boomstuff, bosses[bossloop], boomsound, boomchannel);
	        bosses[bossloop] = object_out(bosses[bossloop]);
	        score = score + 75;
	      };
            };
	  };
        };
	laser--;
      } else {
        //shot sound
        if(sound==1){shotchannel = play_sound_effect(shotsound, shotchannel);};
	//get next inactive bullet, give it player's direction, speed, height, location +speed in direction, active.
	for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
	  if(bullets[bulletloop].active==0){ 
	    bullets[bulletloop].active=1;
	    if(player.face==0){ bullets[bulletloop].direction = 4; };
	    if(player.face==1){ bullets[bulletloop].direction = 6; };
	    bullets[bulletloop].speed = player.speed + 2;
	    bullets[bulletloop].x = player.x;
	    int multiplier = 1;
	    if(player.direction == 4){multiplier = -1; };
	    bullets[bulletloop].y = player.y + (player.speed * multiplier);
	    break;
	  };
	};
	if(tripshot>=1){
	  for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
	    if(bullets[bulletloop].active==0){ 
	      bullets[bulletloop].active=1;
	      if(player.face==0){ bullets[bulletloop].direction = 4; };
	      if(player.face==1){ bullets[bulletloop].direction = 6; };
	      bullets[bulletloop].speed = player.speed + 2;
	      bullets[bulletloop].x = player.x-1;
	      int multiplier = 1;
	      if(player.direction == 4){multiplier = -1; };
	      bullets[bulletloop].y = player.y + (player.speed * multiplier);
	    tripshot--;
	    break;
	    };
	  };
	  for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
	    if(bullets[bulletloop].active==0){ 
	      bullets[bulletloop].active=1;
	      if(player.face==0){ bullets[bulletloop].direction = 4; };
	      if(player.face==1){ bullets[bulletloop].direction = 6; };
	      bullets[bulletloop].speed = player.speed + 2;
	      bullets[bulletloop].x = player.x+1;
	      int multiplier = 1;
	      if(player.direction == 4){multiplier = -1; };
	      bullets[bulletloop].y = player.y + (player.speed * multiplier);
	      tripshot--;
	      break;
	    };
	  };
	};
      };
    };
  
  
    //check for / process smartbomb
    if(input=='a'&&smartbombs>=1){
      smartbombs--;
      for(int landerloop = 0; landerloop<landermax; landerloop++){
        if(landers[landerloop].active==1){
	  if((abs(player.y-landers[landerloop].y)<=40)||(abs(player.y-landers[landerloop].y)>=580)){
	    determine_powerup(powerups, landers[landerloop], 800, pupcreatesound, pupcreatechannel);
	    drawlocation = boom_object(drawlocation, boomstuff, landers[landerloop], boomsound, boomchannel);
	    landers[landerloop] = object_out(landers[landerloop]);
	    score = score + 5;
	  };
        };
      };
      for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
        if(crazies[crazyloop].active==1){
	  if((abs(player.y-crazies[crazyloop].y)<=40)||(abs(player.y-crazies[crazyloop].y)>=580)){
	    drawlocation = boom_object(drawlocation, boomstuff, crazies[crazyloop], boomsound, boomchannel);
	    crazies[crazyloop] = object_out(crazies[crazyloop]);
	    score = score + 5;
	  };
        };
      };
      for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
        if(crawlers[crawlerloop].active==1){
	  if((abs(player.y-crawlers[crawlerloop].y)<=40)||(abs(player.y-crawlers[crawlerloop].y)>=580)){
	    drawlocation = boom_object(drawlocation, boomstuff, crawlers[crawlerloop], boomsound, boomchannel);
	    crawlers[crawlerloop] = object_out(crawlers[crawlerloop]);
	    score = score + 5;
	  };
        };
      };
      for(int bossloop = 0; bossloop<6; bossloop++){
        if(bosses[bossloop].active==1){
	  if((abs(player.y-bosses[bossloop].y)<=40)||(abs(player.y-bosses[bossloop].y)>=580)){
	    bosses[bossloop].phase = bosses[bossloop].phase - 5;
	    if(bosses[bossloop].phase<=0){
	      drawlocation = boom_object(drawlocation, boomstuff, bosses[bossloop], boomsound, boomchannel);
	      bosses[bossloop] = object_out(bosses[bossloop]);
	      score = score + 50;
	    };
	  };
        };
      };
    };
 
    //for each active bullet
    for(int bulletloop = 0; bulletloop<bulletmax; bulletloop++){ 
      if(bullets[bulletloop].active==1){ 
	//check each active pod
	for(int podloop = 0; podloop<podmax; podloop++){
	  if(pods[podloop].active==1){
      	    if(check_collision(bullets[bulletloop], pods[podloop])==1){
	      //kill pod and bullet
	      pods[podloop] = object_out(pods[podloop]);
	      bullets[bulletloop] = object_out(bullets[bulletloop]);
	    };
	  };
	};
	//check each active lander
	for(int landerloop = 0; landerloop<landermax; landerloop++){
	  if(landers[landerloop].active==1){
      	    if(check_collision(bullets[bulletloop], landers[landerloop])==1){
	      //kill lander and bullet
      	      determine_powerup(powerups, landers[landerloop], 800, pupcreatesound, pupcreatechannel);
	      drawlocation = boom_object(drawlocation, boomstuff, landers[landerloop], boomsound, boomchannel);
	      landers[landerloop] = object_out(landers[landerloop]);
	      bullets[bulletloop] = object_out(bullets[bulletloop]);
	      //add to score
	      score = score + 20;
	    };
	  };
	};
	//check each active crazy
	for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
	  if(crazies[crazyloop].active==1){
      	    if(check_collision(bullets[bulletloop], crazies[crazyloop])==1){
	      //kill crazy and bullet
      	      determine_powerup(powerups, crazies[crazyloop], 800, pupcreatesound, pupcreatechannel);	      
	      drawlocation = boom_object(drawlocation, boomstuff, crazies[crazyloop], boomsound, boomchannel);
	      crazies[crazyloop] = object_out(crazies[crazyloop]);
	      bullets[bulletloop] = object_out(bullets[bulletloop]);
	      //add to score
	      score = score + 20;
	    };
	  };
	};
	//check each active crawler
	for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
	  if(crawlers[crawlerloop].active==1){
      	    if(check_collision(bullets[bulletloop], crawlers[crawlerloop])==1){
	      //kill crawler and bullet
      	      determine_powerup(powerups, crawlers[crawlerloop], 800, pupcreatesound, pupcreatechannel);
	      drawlocation = boom_object(drawlocation, boomstuff, crawlers[crawlerloop], boomsound, boomchannel);
	      crawlers[crawlerloop] = object_out(crawlers[crawlerloop]);
	      bullets[bulletloop] = object_out(bullets[bulletloop]);
	      //add to score
	      score = score + 20;
	    };
	  };
	};
	//check bosses
	for(int bossloop = 0; bossloop<6; bossloop++){
	  if(bosses[bossloop].active==1){
	    if(check_collision(bullets[bulletloop], bosses[bossloop])==1){
	      //hurt or kill boss, kill bullet
	      bosses[bossloop].phase = bosses[bossloop].phase - 1;
	      if(bosses[bossloop].phase<=0){
	        determine_powerup(powerups, bosses[bossloop], 600, pupcreatesound, pupcreatechannel);
	        drawlocation = boom_object(drawlocation, boomstuff, bosses[bossloop], boomsound, boomchannel);
	        bosses[bossloop] = object_out(bosses[bossloop]);
	        bullets[bulletloop] = object_out(bullets[bulletloop]);
	        //add to score
	        score = score + 100;
	      };
	    };
	  };
        };
      };
    };
    
    //watch for lander/player collisions
    for(int landerloop = 0; landerloop<landermax; landerloop++){
      if(landers[landerloop].active==1){
	if(check_collision(player, landers[landerloop])==1){
	  //kill lander and player
	  landers[landerloop] = object_out(landers[landerloop]);
	  lives--;
	  //add to score
	  score = score + 10;
	  drawlocation = boom_object(drawlocation, boomstuff, player, boomsound, boomchannel);
	  newgame = life_loss(lives, score);
	  player = player_init(player);
	  drawlocation = player.y-20;
	  //kill any carried pods
	  if(pod_in>0){
	    for(int podloop = 0; podloop<podmax; podloop++){ 
	      if(pods[podloop].number==pod_in){
		pods[podloop] = object_out(pods[podloop]);
		pod_in = 0;
		strcpy (player.line0, "<==_>");
		strcpy (player.line1, "<_==>");
	      };
	    };
	  };
	};
	if(shieldup==1&&super_shield>0){
	  for(int shieldloop = 0; shieldloop<12; shieldloop++){
	    if(check_collision(landers[landerloop], shields[shieldloop])==1){
	      drawlocation = boom_object(drawlocation, boomstuff, landers[landerloop], boomsound, boomchannel);
	      landers[landerloop] = object_out(landers[landerloop]);
	      score = score + 10;
	    };
	  };
	};
      };
    };

    //watch for crazy/player collisions
    for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
      if(crazies[crazyloop].active==1){
	if(check_collision(player, crazies[crazyloop])==1){
	  //kill crazy and player
	  crazies[crazyloop] = object_out(crazies[crazyloop]);
	  lives--;
	  //add to score
	  score = score + 10;
	  drawlocation = boom_object(drawlocation, boomstuff, player, boomsound, boomchannel);
	  newgame = life_loss(lives, score);
	  player = player_init(player);
	  drawlocation = player.y-20;
	  //kill any carried pods
	  if(pod_in>0){
	    for(int podloop = 0; podloop<podmax; podloop++){ 
	      if(pods[podloop].number==pod_in){
		pods[podloop] = object_out(pods[podloop]);
		pod_in = 0;
		strcpy (player.line0, "<==_>");
		strcpy (player.line1, "<_==>");
	      };
	    };
	  };
	};
	if(shieldup==1&&super_shield>0){
	  for(int shieldloop = 0; shieldloop<12; shieldloop++){
	    if(check_collision(crazies[crazyloop], shields[shieldloop])==1){
	      drawlocation = boom_object(drawlocation, boomstuff, crazies[crazyloop], boomsound, boomchannel);
	      crazies[crazyloop] = object_out(crazies[crazyloop]);
	      score = score + 10;
	    };
	  };
	};
      };
    };

    //watch for crawler/player collisions
    for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
      if(crawlers[crawlerloop].active==1){
	if(check_collision(player, crawlers[crawlerloop])==1){
	  //kill crawler and player
	  crawlers[crawlerloop] = object_out(crawlers[crawlerloop]);
	  lives--;
	  //add to score
	  score = score + 10;
	  drawlocation = boom_object(drawlocation, boomstuff, player, boomsound, boomchannel);
	  newgame = life_loss(lives, score);
	  player = player_init(player);
	  drawlocation = player.y-20;
	  //kill any carried pods
	  if(pod_in>0){
	    for(int podloop = 0; podloop<podmax; podloop++){ 
	      if(pods[podloop].number==pod_in){
		pods[podloop] = object_out(pods[podloop]);
		pod_in = 0;
		strcpy (player.line0, "<==_>");
		strcpy (player.line1, "<_==>");
	      };
	    };
	  };
	};
	if(shieldup==1&&super_shield>0){
	  for(int shieldloop = 0; shieldloop<12; shieldloop++){
	    if(check_collision(crawlers[crawlerloop], shields[shieldloop])==1){
	      drawlocation = boom_object(drawlocation, boomstuff, crawlers[crawlerloop], boomsound, boomchannel);
	      crawlers[crawlerloop] = object_out(crawlers[crawlerloop]);
	      score = score + 10;
	    };
	  };
	};
      };
    };

    //watch for boss/player collisions
    for(int bossloop = 0; bossloop<6; bossloop++){
      if(bosses[bossloop].active == 1){
        if(check_collision(player, bosses[bossloop])==1){
	  //kill player only
	  lives--;
	  drawlocation = boom_object(drawlocation, boomstuff, player, boomsound, boomchannel);
	  newgame = life_loss(lives, score);
	  player = player_init(player);
	  drawlocation = player.y-20;
	  //kill any carried pods
	  if(pod_in>0){
	    for(int podloop = 0; podloop<podmax; podloop++){ 
	      if(pods[podloop].number==pod_in){
	        pods[podloop] = object_out(pods[podloop]);
	        pod_in = 0;
	        strcpy (player.line0, "<==_>");
	        strcpy (player.line1, "<_==>");
	      };
	    };
	  };
        };
        //Shields don't hurt bosses!  Tee hee. . .
      };
    };
    //watch for missile/lander collisions
    for(int missileloop = 0; missileloop<missilemax; missileloop++){
      if(missiles[missileloop].active == 1){
        if(check_collision(landers[missiles[missileloop].chase], missiles[missileloop])==1){
	  //kill missile and lander
	  drawlocation = boom_object(drawlocation, boomstuff, landers[missiles[missileloop].chase], boomsound, boomchannel);
	  drawlocation = boom_object(drawlocation, boomstuff, missiles[missileloop], boomsound, boomchannel);
	  landers[missiles[missileloop].chase] = object_out(landers[missiles[missileloop].chase]);
	  missiles[missileloop] = object_out(missiles[missileloop]);
        };
      };
    };
    
    // Lander shots
    for(int landerloop = 0; landerloop<landermax; landerloop++){
      if(landers[landerloop].active==1&&landers[landerloop].chase<=0){
	if(rand()%1000>500){
	  // lander shoot
	  for(int landershotloop = 0; landershotloop<landershotmax; landershotloop++){ 
	    if(landershot[landershotloop].active==0){
	      landershot[landershotloop] = enemy_shoot(landers[landerloop], player, landershot[landershotloop], enshotchannel, enshotsound);
	      break;
	    };
	  };
	};
      };
    };

    // Crazy shots
    for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
      if(crazies[crazyloop].active==1){
	if(rand()%1000>200){
	  // crazy shoot
	  for(int crazyshotloop = 0; crazyshotloop<landershotmax; crazyshotloop++){ 
	    if(landershot[crazyshotloop].active==0){ 
	      landershot[crazyshotloop] = enemy_shoot(crazies[crazyloop], player, landershot[crazyshotloop], enshotchannel, enshotsound);
	      break;
	    };
	  };
	};
      };
    };

    // Crawler shots
    for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){
      if(crawlers[crawlerloop].active==1&&crawlers[crawlerloop].chase<=0){
	if(rand()%1000>500){
	  // crawler shoot
	  for(int crawlershotloop = 0; crawlershotloop<landershotmax; crawlershotloop++){ 
	    if(landershot[crawlershotloop].active==0){ 
	      landershot[crawlershotloop] = enemy_shoot(crawlers[crawlerloop], player, landershot[crawlershotloop], enshotchannel, enshotsound);
	      break;
	    };
	  };
	};
      };
    };

    // boss shots
    for(int bossloop = 0; bossloop<6; bossloop++){
      if(bosses[bossloop].active==1){
        if(rand()%1000>100){
	  // boss shoot
	  for(int bossshotloop = 0; bossshotloop<landershotmax; bossshotloop++){ 
	    if(landershot[bossshotloop].active==0){ 
	      landershot[bossshotloop] = enemy_shoot(bosses[bossloop], player, landershot[bossshotloop], enshotchannel, enshotsound);
	      break;
	    };
	  };
        };
      };
    };
    
    //for each active lander shot
    for(int landershotloop = 0; landershotloop<landershotmax; landershotloop++){ 
      if(landershot[landershotloop].active==1){ 
	if(check_collision(landershot[landershotloop], player)==1){
	  //kill landershot and player
	  lives--;
	  landershot[landershotloop] = object_out(landershot[landershotloop]);  
	  drawlocation = boom_object(drawlocation, boomstuff, player, boomsound, boomchannel);
	  newgame = life_loss(lives, score);
	  player = player_init(player);
	  drawlocation = player.y-20;
	  //kill any carried pods
	  if(pod_in>0){
	    for(int podloop = 0; podloop<podmax; podloop++){ 
	      if(pods[podloop].number==pod_in){
		pods[podloop] = object_out(pods[podloop]);
		pod_in = 0;
		strcpy (player.line0, "<==_>");
		strcpy (player.line1, "<_==>");
	      };
	    };
	  };
	};
	if(shieldup==1){
	  for(int shieldloop = 0; shieldloop<12; shieldloop++){
	    if(check_collision(landershot[landershotloop], shields[shieldloop])==1){
	      landershot[landershotloop] = object_out(landershot[landershotloop]);
	    };
	  };
	};
      };
    };

    //start new game if applicable
    if(newgame==1){
      halfdelay(1);
      //Reset lives
      lives = 4;
      //Reset level
      level = 1;
      //Drop shields
      shieldup = 0;
      //Zero laser
      laser = 0;
      //Zero triple shot
      tripshot = 0;
      //Reactivate pods
      for(int podloop = 0; podloop<podmax; podloop++){
        if(pods[podloop].active==0){
          pods[podloop] = pod_init(pods[podloop]);
        };
      };
      podsin = 0;
      pod_in = 0;
      strcpy (player.line0, "<==_>");
      strcpy (player.line1, "<_==>");
      //Bump shields
      shieldsleft = 500;
      //Smart bomb
      smartbombs = 3;
      //regen landers
      int landerlimit = 4;
      for(int landerloop = 0; landerloop<landerlimit; landerloop++){
        if(landers[landerloop].active==0){
          landers[landerloop] = lander_init(landers[landerloop]);
        };
      };
      //regen crawlers
      int crawlerlimit = 2;
      for(int crawlerloop = 0; crawlerloop<crawlerlimit; crawlerloop++){
        if(crawlers[crawlerloop].active==0){
          crawlers[crawlerloop] = crawler_init(crawlers[crawlerloop]);
        };
      };
      //Deactivate powerups
      for(int poweruploop = 0; poweruploop<4; poweruploop++){
        powerups[poweruploop].active = 0;
      };
      //Deactivate crazies
      for(int crazyloop = 0; crazyloop<landermax; crazyloop++){
        crazies[crazyloop].active = 0;
      };
      //Deactivate bosses
      for(int bossloop = 0; bossloop<6; bossloop++){
        bosses[bossloop].active = 0;
      };
      score = 0;
      newgame = 0;
    };
  
    //lander pod grab logic
    //steps:
    //decide to grab a pod
    for(int landerloop = 0; landerloop<landermax; landerloop++){
      if(landers[landerloop].active==1&&landers[landerloop].chase==-1){
	//if(rand()%10000<=10){
	if(rand()%1000<=10){
	  //choose a pod
	  for(int podloop = 0; podloop<podmax; podloop++){
	    if(landers[landerloop].chase==-1&&pods[podloop].chase==-1&&pods[podloop].active==1){
	      landers[landerloop].chase = podloop;
	      pods[podloop].chase = landerloop;
	    };
	  };
	  //alert player  print >>>> or <<<< at mid height, flash.  set a value, have a function do it in the draw area.
	  //travel to it at speed 3
	  landers[landerloop].speed = 3;
	  if(player.y<landers[landerloop].y){ alertright = 20;};
	  if(player.y>=landers[landerloop].y){ alertleft = 20;};
	  if(((pods[landers[landerloop].chase].y<landers[landerloop].y)&&(abs(pods[landers[landerloop].chase].y-landers[landerloop].y)<310))||((pods[landers[landerloop].chase].y>landers[landerloop].y)&&(abs(pods[landers[landerloop].chase].y-landers[landerloop].y)>310))){  
	    landers[landerloop].direction = 4;
	  };
	  if(((pods[landers[landerloop].chase].y>landers[landerloop].y)&&(abs(pods[landers[landerloop].chase].y-landers[landerloop].y)<310))||((pods[landers[landerloop].chase].y<landers[landerloop].y)&&(abs(pods[landers[landerloop].chase].y-landers[landerloop].y)>310))){  
	    landers[landerloop].direction = 6;
	  };
	  landers[landerloop].phase = 1;
	  //launch missile if we have one
	  if(missile>=1){
            for(int missileloop = 0; missileloop<missilemax; missileloop++){
              if(missiles[missileloop].chase==-1){
                missiles[missileloop].chase = landerloop;
                //play missle launch sound
                missilechannel = play_sound_effect(missilesound, missilechannel);
                //init missle
                missiles[missileloop] = missile_init(missiles[missileloop], player, landers[landerloop]);
                missile--;
              };   
            };                                                                        
          };
	  //strcpy(landers[landerloop].radar, "$"); //debug
	  //strcpy(pods[landers[landerloop].chase].radar, "&"); //debug
	};
      };
    };

    //check for 0 active pods, saved pods, level victory
    podcount = 0;
    for(int podloop = 0; podloop<podmax; podloop++){ podcount = podcount + pods[podloop].active; };
    if(podcount<=0){
      if(podsin<=0&&landercount>0){
        //crazy the landers
        for(int landerloop = 0; landerloop<landermax; landerloop++){ 
          if(landers[landerloop].active==1){
            crazies[landerloop] = encrazify(landers[landerloop], crazies[landerloop], crazifysound, crazifychannel);
            landers[landerloop] = object_out(landers[landerloop]);
          };
        };
      };
      //do the level victory once all landers are dead  
      landercount = 0;
      for(int landerloop = 0; landerloop<landermax; landerloop++){ landercount = landercount + landers[landerloop].active; };
      //do the level victory once all crazies are dead  
      crazycount = 0;
      for(int crazyloop = 0; crazyloop<landermax; crazyloop++){ crazycount = crazycount + crazies[crazyloop].active; };
      crawlercount = 0;
      for(int crawlerloop = 0; crawlerloop<crawlermax; crawlerloop++){ crawlercount = crawlercount + crawlers[crawlerloop].active; };
      if(crazycount<=0&&crawlercount<=0){
        bosscount = 0;
        for(int bossloop = 0; bossloop<6; bossloop++){ bosscount = bosscount + bosses[bossloop].active; };
	if(landercount<=0&&bosscount<=0){
	  levelendchannel = play_sound_effect(levelendsound, levelendchannel);
	  //Tally bonuses
	  //Award lives, if any
	  if(lives<4){lives++;};
	  //Print goodies, getch
	  pause_game = 0;
	  while(pause_game!=' '){
	    cbreak();
	    mvprintw(10,20,"Level %d Completed.  Press SPACE to continue.\n", level);
	    pause_game = getch();
	  };
	  if(level==24){
	    pause_game = 'f';
	    while(pause_game!=' '){
	      mvprintw(11,21,"Victory!!!! Score: %d Press SPACE to exit\n", score);
	      pause_game = getch();
	      loopvar = 1;
            };
	  };

	  halfdelay(1);
	  //Advance level
	  level++;
	  //Drop shields
	  shieldup = 0;
	  //Reactivate pods if not zombied
	  if((level-1) % 4 == 0){
	    for(int podloop = 0; podloop<podmax; podloop++){ pods[podloop] = pod_init(pods[podloop]); };
	  } else {
	    for(int podloop = 0; podloop<podsin; podloop++){ pods[podloop] = pod_init(pods[podloop]); };
	  };
	  podsin = 0;
	  //Bump shields
	  if(shieldsleft<=450){
	    shieldsleft = shieldsleft+50;
	  } else {
	    shieldsleft = 500;
	  };
	  //Smart bomb
	  if(smartbombs<=3){smartbombs++;};
	  //regen landers
          int landerlimit = 0;
          if(level<=4){landerlimit=4;};
          if(level>4&&level<=12){landerlimit=6;};
          if(level>12&&level<=16){landerlimit=8;};
          if(level>16&&level<=20){landerlimit=10;};          
          if(level>20){landerlimit=12;};                    
	  
	  for(int landerloop = 0; landerloop<landerlimit; landerloop++){ 
	    if(landers[landerloop].active==0){
	      landers[landerloop] = lander_init(landers[landerloop]);
	    };
	  };
	  //regen crawlers
	  int crawlerlimit = 0;
          if(level<=8){crawlerlimit=2;};
          if(level>8&&level<=16){crawlerlimit=4;};
          if(level>16){crawlerlimit=6;};                    
	  
	  for(int crawlerloop = 0; crawlerloop<crawlerlimit; crawlerloop++){ 
	    if(crawlers[crawlerloop].active==0){
	      crawlers[crawlerloop] = crawler_init(crawlers[crawlerloop]);
	    };
	  };
	  //Gen bosses
	  if(level==4){
	    bosses[0] = boss_init(bosses[0], level);
	  };
          if(level==8){
	    bosses[1] = boss_init(bosses[1], level);
	  };
	  if(level==12){
	    bosses[2] = boss_init(bosses[2], level);
	  };
	  if(level==16){
	    bosses[3] = boss_init(bosses[3], level);
	  };
	  if(level==20){
	    bosses[4] = boss_init(bosses[4], level);
	  };
	  if(level==24){
	    bosses[5] = boss_init(bosses[5], level);
	  };
	};
      };  
    };
    
  }; //end main loop

  if(sound==1){
    Mix_FreeChunk(shotsound);
    Mix_FreeChunk(boomsound);
    Mix_FreeChunk(dropoffsound);
    Mix_FreeChunk(pickupsound);
    Mix_FreeChunk(lasersound);
    Mix_FreeChunk(warningsound);
    Mix_FreeChunk(pupcreatesound);
    Mix_FreeChunk(puptakesound);
    Mix_FreeChunk(shieldupsound);
    Mix_FreeChunk(shielddownsound);
    Mix_FreeChunk(crazifysound);
    Mix_FreeChunk(enshotsound);
    Mix_FreeChunk(missilesound);
    Mix_FreeChunk(levelendsound);

    //no need to free title music, done after title.

    Mix_CloseAudio();
    SDL_Quit();
  };
  
  endwin();
  
  return 0;  
}
