#include <cstdlib>
#include <string.h>
#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

#include "game_object.h"
#include "play_sound_effect.h"

game_object powerup_init(game_object powerup, const game_object& object){
  //Call immediately before an object_out
  powerup.active = 1;
  powerup.x = object.x;
  powerup.y = object.y;
  if(rand()%1000>=500){
      powerup.direction = 4;
    } else {
      powerup.direction = 6;
    };
  powerup.speed = 1;
  
  int fodder = rand()%1500;

  if(fodder>=1200){
    strncpy(powerup.line0, "M", 2);
  };
  if(fodder>=1000&&fodder<1200){
    strncpy(powerup.line0, "T", 2);
  };
  if(fodder>=800&&fodder<1000){
    strncpy(powerup.line0, "S", 2);
  };
  if(fodder>=600&&fodder<800){
    strncpy(powerup.line0, "K", 2);
  };
  if(fodder>=400&&fodder<600){
    strncpy(powerup.line0, "L", 2);
  };
  if(fodder>=200&&fodder<400){
    strncpy(powerup.line0, "B", 2);
  };
  if(fodder<200){
    strncpy(powerup.line0, "Z", 2);
  };
  
  return powerup;
};

void determine_powerup(game_object powerups[], const game_object& object, int odds, Mix_Chunk *sound, int channel){
  if(rand()%1000>odds){
    for(int puploop = 0; puploop<4; puploop++){
      if(powerups[puploop].active==0){
        play_sound_effect(sound, channel);
        powerups[puploop] = powerup_init(powerups[puploop], object);
	break;
      };
    };
  };

};
