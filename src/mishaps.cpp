#include <ncurses.h>
#include <math.h>
#include <unistd.h>
#include <cstring>
#include "SDL2/SDL.h"
#include "SDL2/SDL_mixer.h"

#include "game_object.h"
#include "draw.h"
#include "play_sound_effect.h"

game_object encrazify(const game_object& lander, game_object crazy, Mix_Chunk *sound, int channel){
  play_sound_effect(sound, channel);
  crazy.active=1;
  crazy.x = lander.x;
  crazy.y = lander.y;  
  crazy.face = lander.face;
  crazy.vspeed = 0;
  crazy.vtime = lander.vtime;
  crazy.direction = lander.direction;
  crazy.speed = 1;

  return crazy;
}

int boom_object(int drawlocation, game_object boomstuff, const game_object& object, Mix_Chunk *sound, int channel){

  int deathcycle=1;
  int loop=1;
  boomstuff.active=1;
  play_sound_effect(sound, channel);
  while(deathcycle<=7){
    for(int boomx=object.x-10;boomx<=object.x+10;boomx++){
      for(int boomy=object.y-10;boomy<=object.y+10;boomy++){
	if(sqrt((abs(object.y-boomy)*abs(object.y-boomy))+(abs(object.x-boomx)*abs(object.x-boomx)))<=deathcycle){
	  if(rand()%1000>900){
	    boomstuff.x = boomx;
	    boomstuff.y = boomy;
	    if(rand()%1000>500){
	      boomstuff.face = 0;
	    } else {
	      boomstuff.face = 1;
	    };
	    for(int iterations=0;iterations<=loop;iterations++){
	      drawlocation = draw_object(boomstuff, drawlocation);
	    };
	    //get the cursor out of the way
	    mvprintw(23,79,"-");
	    refresh();
	  };
	};
      };
    };
    deathcycle++;
  };
  if(object.number!=1){
    usleep(50000);
  };

  return drawlocation;
}

int life_loss(int lives, int score){

  int newgame = 0;

  int pause_game;

  if(lives>=1){
    pause_game = 0;
    while(pause_game!=' '){
      cbreak();
      mvprintw(10,20,"Boom.  Press SPACE to continue.\n");
      pause_game = getch();
    };
    halfdelay(1);
  } else {
    pause_game = 0;
    
    //record score if high
    
    int hscore = 0;
    char filename[250];
    char outstring[50];
    strcpy (filename, "/.curblaster_scores");
    char home[250];
    strncpy(home, getenv("HOME"), 230);
    
    strncat(home, filename, 20);
    
    FILE *scorefile = fopen(home, "r");
    if(scorefile != NULL){
      char workchara[6];
      if(fgets(workchara, 6, scorefile) != NULL){
        hscore = atoi(workchara);
      };
      fclose(scorefile);
    };
    
    if(score>hscore){
      FILE *writescorefile = fopen(home, "w");
      if(writescorefile != NULL){
        sprintf(outstring, "%d", score);
        fputs(outstring, writescorefile);
        fclose(writescorefile);
      };
    };
    
    while(pause_game!='y'&&pause_game!='n'){
      cbreak();
      mvprintw(10,20,"GAME OVER. Score:%d  Play again(y/n)?\n", score);
      if(score>hscore){
	mvprintw(11,20,"High Score: %s", outstring);
      };
      pause_game = getch();
    };
    if(pause_game=='n'){
      endwin();
      exit(0);
    } else {
      newgame = 1;
    };
  };

  return newgame;
}
