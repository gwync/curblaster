# NAME

curblaster - NCurses sidescroller, with gameplay loosely based on a few
others.

# SYNOPSIS

**curblaster**

Grab pods and drop them in the gate, while fighting enemies in your way.

# DESCRIPTION

Get as many pods as you can through the gate, avoiding landers,
crawlers, and possibly even a large flying saucer.

Written to assuage my longing for my long-deceased Amiga.

# CONTROLS

Movement: Arrow keys

Shoot: Z

Shield toggle: S

Smart bomb: A

Pause: Space

Quit: Q

# RESOURCES

Requires ncurses, and SDL_mixer for sound.

# COPYRIGHT

Curblaster is Copyright (c) 2022 Gwyn Ciesla

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program. If not, see \<http://www.gnu.org/licenses/>.

# BUGS

None so far, but looking to improve gameplay.

# AUTHORS

Gwyn Ciesla E-mail : gwync at protonmail dot com
